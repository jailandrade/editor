import React from 'react';
import styled from 'styled-components';
import '../App.css';
import diff from '../lib/htmldiff';

const Container = styled.div`
  display: block;
  padding: 40px;
  font-size: 18px;
`;

const Diff = (props) => {
  const first = props.first;
  const second = props.second;

  const createMarkup = (html) => {
    return {__html: html} 
  }

  return (
	  <Container>
      <div dangerouslySetInnerHTML={createMarkup(diff(first,second))}></div>
    </Container>
  );
}

export default Diff;

