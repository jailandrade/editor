import React from 'react';
import styled from 'styled-components';
import Diff from './components/Diff';
import EditorContainer from './Editor';

const Container = styled.div`
  display: block;
`;

let first = '<div class="container"><p>Colors bounced around in her head. They mixed and threaded themselves together. Even colors that had no business being together. They were all one, yet distinctly separate at the same time. How awas she going to explain this to the others?</p><p>The boy walked down the street in a carefree way, playing without notice of what was about him. He didn\'t hear the sound of the card as his ball careened into the road. He took a step toward it, and in doing so sealed his fate.</p></div>';

  let second = '<div class="container"><p>Colors bounced around in her head. and threaded Here is some new text together. Even colors that had no business being together. They were all one, yet distinctly separate at the same time. How awas she going to explain this to the others?</p><p>The boy walked down the street in a carefree way, I am adding new text now playing without notice of what was about him. He didn\'t hear the sound of the card as his ball careened into the road. He took a step toward it, and in doing so sealed his fate.</p></div>';

function App() {
  return (
	  <Container>
      <Diff first={first} second={second}/>
      <EditorContainer /> 
    </Container>
  );
}

export default App;
