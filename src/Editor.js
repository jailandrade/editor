import React, { useState, useEffect, useRef } from 'react';
import './App.css';
import { Editor } from '@tinymce/tinymce-react';
import styled from 'styled-components';
import diff from './lib/htmldiff.js';

const Container = styled.div`
  display: grid;
  grid-template-columns: 4fr 1fr;
`;

function EditorContainer() {
  const [content, setContent] = useState('');
  const [textdiff, setTextdiff] = useState('');

  const handleEditorChange = (newContent, editor) => {

    setContent(newContent); 

    if (newContent, content) {
      console.log(diff(content, newContent));
      setTextdiff(diff(content, newContent)); 
    }
  }

  const handleOnBlur = (val) => {
    console.log('blur ', val);
  }

  const createMarkup = (html) => {
    return {__html: html};
  }

  return (
	  <Container>
      <Editor
        apiKey='xd6or7qwybgv20pf0a6z4oogpey8z6wz8yk5qlavunk7bdbv'
        initialValue="<p>Write here</p>"
        init={{
          height: 500,
          menubar: false,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
          ],
          toolbar: 
            'undo redo | formatselect | bold italic blackcolor | \
            alignleft aligncenter alignright alignjustify | \
            bullist numlist outdent indent | removeformat | help'
          }}
          onEditorChange={handleEditorChange}
          onBlur={handleOnBlur}
      />
      <div>
        <div>Latest changes</div>
        <div dangerouslySetInnerHTML={createMarkup(textdiff)}></div>
      </div>
    </Container>
  );
}

export default EditorContainer;
